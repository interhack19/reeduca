const routes = [
  {
    path: '/',
    component: () => import('layouts/Blank.vue'),
    children: [
      { path: '', component: () => import('pages/Index.vue') },
      { path: 'test', component: () => import('pages/ComponentTester.vue') }
    ]
  },
  {
    path: '/app',
    component: () => import('layouts/TabLayout.vue'),
    redirect: '/app/simulado',
    children: [
      {
        path: 'apostila',
        component: () => import('pages/app/Apostila.vue')
      },
      {
        path: 'content/:subject',
        component: () => import('pages/app/content/Subject.vue')
      },
      {
        path: 'revisao',
        component: () => import('pages/app/Revisao.vue')
      },
      {
        path: 'simulado',
        component: () => import('pages/app/Simulado.vue')
      },
      {
        path: 'simulado/:type',
        component: () => import('pages/app/FazSimulado.vue')
      }
    ]
  }
];

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  });
}

export default routes;
