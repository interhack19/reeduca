import { LocalStorage, SessionStorage } from 'quasar';
const defaultData = require('../../statics/reference/reference.json');
let localData = LocalStorage.getItem('reeduca-quasar');
if (!localData) localData = defaultData;

export default {
  appName: 'Reeduca',
  refData: localData,
  defaultData
};
