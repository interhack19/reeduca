/*
export function someMutation (state) {
}
*/
import { LocalStorage, SessionStorage } from 'quasar';

export function setLocalData(state) {
  LocalStorage.set('reeduca-quasar', state.refData);
}
