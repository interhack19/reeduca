/*
export function someGetter (state) {
}
*/
import { LocalStorage, SessionStorage } from 'quasar';

export function getData(state) {
  let tmp = LocalStorage.getItem('reeduca-quasar');
  if (state.refData == null && tmp != null) state.refData = tmp;
  return state.refData;
}
